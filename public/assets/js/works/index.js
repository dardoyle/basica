$(function(){
  let scroll = false;

  $('#works_index_more').click(function(e){
    e.preventDefault();
    let offset = $('#works-list .col-sm-6').length;
    let limit = (typeof $(this).data('limit') !== typeof undefined && $(this).data('limit') !== false)? $(this).data('limit') : 10;
    $.get($(this).data('url'), {
              offset,
              limit: $(this).data('limit'),
              beforeSend: function()
              {
                $('.ajax-load').show();
              }

          },function(reponse){
             if(reponse == " "){
               $('.ajax-load').html("il n'y a plus d'enregistrements");
               return;
             }
             $('.ajax-load').hide();
             $('#works-list').append(reponse)
                             .find('.col-sm-6:nth-last-of-type(-n + '+ limit +' )')
                             .fadeIn();
          scroll = false;
    });
  });

  $(window).scroll(function(){
    if(($(window).scrollTop() + $(window).height() > $(document).height() - 100) && scroll == false){
      scroll = true;
      $('#works_index_more').click();
    }
  });
});
