{{-----------------------------------------------------------------
 ./ressources/views/public/posts/_recents.blade.php
 liste des posts récentst

 Variables disponibles : $posts (tableau d'ojets de type Post)
------------------------------------------------------------------------------}}
 <h4>Recent Posts</h4>
 <ul class="recent-posts">
   @foreach($posts as $post)
     <li><a href="{{ route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->title, '-')]) }}">{{ $post->title }}</a></li>
   @endforeach
 </ul>
