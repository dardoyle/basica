<form method="POST" action="{{ route('posts.store') }}">
    @csrf
  <div>
    <label for="name">Name</label>
    <input type="text" id="name" name="name" />
    <label for="content">Content</label>
    <textarea id="content" name="content" rows="4" cols="50"/></textarea>
  </div>
  <div>
    <input type="submit" />
  </div>
</form>
