{{-----------------------------------------------------------------
 ./ressources/views/public/posts/index.blade.php
 affichage des posts

 Variables disponibles : $posts (tableau d'ojets de type Post)
------------------------------------------------------------------------------}}
@extends('public.templates.template1')

@section('title')
  Blog
@endsection

@section('currentPageTitle')
  Our blog
@endsection

@section('content')

  <div class="section">
  <div class="container">
  <div class="row">

      @foreach($posts as $post)
    <!-- Blog Post Excerpt -->
    <div class="col-sm-6">
      <div class="blog-post blog-single-post">
        <div class="single-post-title">
          <h2>{{ Str::words($post->title, 3, '') }}</h2>
        </div>

        <div class="single-post-image">
          <img src="{{ asset('assets/img/blog/'.$post->image) }}" alt="{{ $post->title }}">
        </div>

        <div class="single-post-info">
          <i class="glyphicon glyphicon-time"></i>{{ \Carbon\Carbon::parse($post->created_at)->format('d M, Y') }}<a href="#" title="Show Comments"><i class="glyphicon glyphicon-comment"></i>11</a>
        </div>

        <div class="single-post-content">
          <p>
            {{ Str::words($post->content, 30, '. ') }}
          </p>
        <a href="{{ route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->title, '-')]) }}" class="btn">Read more</a>
        </div>
      </div>
    </div>
    <!-- End Blog Post Excerpt -->
    @endforeach

<div class="pagination-wrapper ">
  {{ $posts->links() }}
</div>

  </div>
</div>
</div>

@endsection
