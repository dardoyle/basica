{{-----------------------------------------------------------------
 ./ressources/views/public/posts/_list.blade.php
 liste des posts

 Variables disponibles : $posts (tableau d'ojets de type Post)
------------------------------------------------------------------------------}}

<div class="section">
  <div class="container">
    <div class="row">
       <div class="col-sm-6 featured-news">
        <h2>Latest Blog Posts</h2>
        @foreach($posts as $post)
          <div class="row">
            <div class="col-xs-4"><a href="{{ route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->title, '-')]) }}"><img src="{{ asset('/assets/img/blog/'.$post->image) }}" alt="$post->title"></a></div>
            <div class="col-xs-8">
              <div class="caption"><a href="{{ route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->title, '-')]) }}">{{ $post->title }}</a></div>
              <div class="date">{{ \Carbon\Carbon::parse($post->created_at)->format('j F Y') }} </div>
              <div class="intro">{{ Str::words($post->content, 14, '. ') }}<a href="{{ route('posts.show', ['post' => $post->id, 'slug' => Str::slug($post->title, '-')]) }}">Read more...</a></div>
            </div>
          </div>
        @endforeach
      </div>
      <div class="col-sm-6 latest-news">
        <h2>Lastest FaceBook/Twitter News</h2>
        <div class="row">
          <div class="col-sm-12">
            <a class="twitter-timeline" href="https://twitter.com/BastinGisele?ref_src=twsrc%5Etfw">Tweets by BastinGisele</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
        </div>

      </div>
      <!-- End Featured News -->
    </div>
  </div>
</div>
    </div>
  </div>
</div>
