{{-----------------------------------------------------------------
 ./ressources/views/public/categories/index.blade.php
 liste des catégories

 Variables disponibles : $categories (tableau d'ojets de type Categorie)
------------------------------------------------------------------------------}}

 <h4>Categories</h4>
 <ul class="blog-categories">
   @foreach ($categories as $categorie)
     <li><a href="#">{{ $categorie->name }}</a></li>
   @endforeach
 </ul>
