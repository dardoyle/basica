{{-----------------------------------------------------------------
 ./ressources/views/public/contacts/infos.blade.php
 ------------------------------------------------------------------------------}}
@extends('public.templates.template1')

@section('title')
  Contact
@endsection

@section('currentPageTitle')
  Contact us
@endsection

@section('content')
  <div class="section section-map">


        <div class="col-sm-12" style="padding:0;">
          <!-- Map -->
          <div id="contact-us-map">
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.8568383384127!2d5.680495615622059!3d50.611197679497835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c0f41c3d702da7%3A0x2d5b3cb7b45da7ab!2sIEPS%20Fl%C3%A9ron!5e0!3m2!1sfr!2sbe!4v1612348546457!5m2!1sfr!2sbe"></iframe>
        <br />
        <small>
          <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2531.8568383384127!2d5.680495615622059!3d50.611197679497835!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c0f41c3d702da7%3A0x2d5b3cb7b45da7ab!2sIEPS%20Fl%C3%A9ron!5e0!3m2!1sfr!2sbe!4v1612348546457!5m2!1sfr!2sbe"></a>
        </small>
        </iframe>
          </div>
          <!-- End Map -->
    </div>


</div>

  <div class="section">
  <div class="container">
      <div class="row">
    <h3>Get in Touch with Us</h3>
    </hr>
        <div class="col-sm-6">
          <!-- Contact Info -->
          <p class="contact-us-details">
            <b>Address:</b> rue Charles Deliège, 9 - 4623 Fléron<br/>
            <b>Phone:</b> +32 4 377 99 99<br/>
            <b>Fax:</b>   +32 123 654321<br/>
            <b>Email:</b> <a href="mailto:info@promotion-sociale.be">info@promotion-sociale.be</a>
          </p>
          <!-- End Contact Info -->
        </div>
        <div class="col-sm-6">

        </div>
      </div>
  </div>
</div>

@endsection
