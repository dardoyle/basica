{{-----------------------------------------------------------------
 ./ressources/views/public/templates/template1.blade.php
 template de l'interface publique
------------------------------------------------------------------------------}}
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<html lang="en">

  <head>
    @include('public.templates.partials._head')
  </head>

  <body>
    @include('public.templates.partials._header')
    @if(!Route::is('homepage'))
      @include('public.templates.partials._breadcrump')
    @endif
    @yield('content')

	   <!-- Footer -->
     @include('public.templates.partials._footer')
     <!-- Javascripts -->
     @include('public.templates.partials._scripts')
   </body>
</html>
