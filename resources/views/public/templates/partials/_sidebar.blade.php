{{-----------------------------------------------------------------
 ./ressources/views/public/templates/partials/_sidebar.blade.php
 affichage de la sidebar
------------------------------------------------------------------------------}}
<!-- Sidebar -->
<div class="col-sm-4 blog-sidebar">
   @include('public.posts._recents', ['posts' => \App\Models\Post::orderBy('created_at', 'DESC')->take(4)->get()])
   @include('public.categories._index', ['categories' => \App\Models\Categorie::orderBy('name', 'ASC')->take(10)->get()])
 </div>
<!-- End Sidebar -->
