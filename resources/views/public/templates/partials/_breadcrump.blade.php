{{-----------------------------------------------------------------
 ./ressources/views/public/templates/partials/_breadcrump.blade.php
 affichage des titres dans les bannières vertes
------------------------------------------------------------------------------}}
<!-- Page Title -->
<div class="section section-breadcrumbs">
<div class="container">
<div class="row">
  <div class="col-md-12">
    <h1>@yield('currentPageTitle')</h1>
  </div>
</div>
</div>
</div>
