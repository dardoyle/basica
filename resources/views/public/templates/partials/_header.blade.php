{{-----------------------------------------------------------------
 ./ressources/views/public/templates/partials/_header.blade.php
 affichage du header
------------------------------------------------------------------------------}}
<header class="navbar navbar-inverse navbar-fixed-top" role="banner">
  <div class="container">
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.html"><img src="{{ asset('assets/img/logo.png') }}" alt="Basica"></a>
      </div>
      <div class="collapse navbar-collapse">
        @include('public.templates.partials._navigation')
      </div>
  </div>
</header>
