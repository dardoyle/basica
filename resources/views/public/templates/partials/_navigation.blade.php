{{-----------------------------------------------------------------
 ./ressources/views/public/templates/partials/_navigation.blade.php
 affichage de la navigation
------------------------------------------------------------------------------}}
<ul class="nav navbar-nav navbar-right">
    <li class = {{ request()->routeIs('homepage') ? 'active' : '' }} ><a href="{{ route('homepage') }}">Home</a></li>
    <li class = {{ request()->routeIs('portfolio') ? 'active' : ''}}><a href="{{ route('portfolio') }}">Portfolio</a></li>
    <li class = {{ request()->routeIs('blog') ? 'active' : ''}}><a href="{{ route('blog') }}">Blog</a></li>
    <li class = {{ request()->routeIs('contact') ? 'active' : ''}}><a href="{{ route('contact') }}">Contact</a></li>
    <li class = {{ request()->routeIs('login') ? 'active' : ''}}><a href="{{ route('login') }}">Login dashboard</a></li>
    <li class = {{ request()->routeIs('register') ? 'active' : ''}}><a href="{{ route('register') }}">Register dashboard</a></li>
</ul>
