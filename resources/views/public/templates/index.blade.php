{{-----------------------------------------------------------------
 ./ressources/views/public/templates/index.blade.php
 affichage de la page d'accueil
------------------------------------------------------------------------------}}
@extends('public.templates.template1')

@section('title')
  Homepage
@endsection

@section('content')
  @include('public.templates.partials._mainSlider')
  <!-- Our Portfolio -->
  @include('public.works._index',  ['works' => \App\Models\Work::orderBy('created_at', 'DESC')->take(6)->get()])

  <hr>
  <!-- Our Articles -->
  @include('public.posts._list', ['posts' => \App\Models\Post::orderBy('created_at', 'DESC')->take(3)->get()])

@endsection
