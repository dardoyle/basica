{{-----------------------------------------------------------------
 ./ressources/views/public/portfolio/index.blade.php
 index des works
------------------------------------------------------------------------------}}
@extends('public.templates.template1')

@section('title')
  Portfolio
@endsection

@section('scripts')
  <script src="{{ asset('assets/js/works/index.js')}}"></script>
@endsection

@section('currentPageTitle')
  Our Portfolio
@endsection

@section('content')

<div class="section">
<div class="container">
<div class="row">
<div class="col-sm-12">
    <h2>We are leading company</h2>
    <h3>Specializing in Wordpress Theme Development</h3>
    <p>
      Donec elementum mi vitae enim fermentum lobortis. In hac habitasse platea dictumst. Ut pellentesque, orci sed mattis consequat, libero ante lacinia arcu, ac porta lacus urna in lorem. Praesent consectetur tristique augue, eget elementum diam suscipit id. Donec elementum mi vitae enim fermentum lobortis.
    </p>

  </div>
</div>
</div>
</div>

<div class="section">
<div class="container">
<div class="row">

<ul class="grid cs-style-2" id="works-list">
  @include('public.works._list')
</ul>


</div>

 @include('public.portfolio._more')

</div>

<div class="ajax-load text-center" style="display:none">
  <p><img src="{{ asset('assets/img/bx_loader.gif')}}">Loading more works</p>
</div>
@endsection
