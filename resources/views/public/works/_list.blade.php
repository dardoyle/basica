@foreach($works as $work)
  <div class="col-md-4 col-sm-6">
<figure>
  <img src="{{ asset('assets/img/portfolio/'.$work->image) }}" alt="{{ $work->title }}">
  <figcaption>
    <h3>{{ $work->client->name }}</h3>
    {{-- voir si SETTINGS était bien le client ?? --}}
    <span>Jacob Cummings</span>
    <a href="{{ route('works.show', ['work' => $work->id, 'slug' => Str::slug($work->title, '-')]) }}">Take a look</a>
  </figcaption>
</figure>
  </div>
@endforeach
