{{-----------------------------------------------------------------
 ./ressources/views/admin/works/addForm.blade.php
 affichage du formulaire d'ajout d'un work

 Variable disponible : $clients (tableau d'ojets de type Client)
---------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  ajout d'un work
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('works.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>
  <form class="" action="{{ route('works.store') }}" method="post">
    @csrf
    <h5 class="text-2xl pb-8">Ajout d'un work</h5>
    <label class="mr-11" for="title">Title</label>
    <input class="mr-6" type="text" name="title" id="title" value="" placeholder="titre du work"><br/>
    <label class="mr-11" for="content">Content</label>
    <textarea name ="content" id="content"  rows="3" cols="30" placeholder="contenu du work"></textarea>
    <div>
      <label class="mr-8" for="client">Client</label>
      <select class="mt-4" name="client" id="client">
        <option value="">Veuillez choisir un client</option>
        @foreach ($clients as $client)
            <option value="{{ $client->id }}" {{ 'selected'}}>{{ $client->name }} </option>
        @endforeach
      </select>
    </div>
    <button class="rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Ajouter</button>
  </form>
 </div>
@endsection
