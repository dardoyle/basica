{{-----------------------------------------------------------------
 ./ressources/views/admin/works/index.blade.php
 liste de works

 Variable disponible : $works (ojet de type Work)
---------------------------------------------------------------}}

@extends('admin.dashboard')

@section('title')
  liste des works
@endsection

@section('content')

  <table class="border-separate border border-blue-300 table-fixed">
    <thead>
      <tr>
        <th class="px-2 border border-blue-300">#</th>
        <th class="border border-blue-300">Titre</th>
        <th class="border border-blue-300">Content</th>
        <th class="border border-blue-300">Tags</th>
        <th class="border border-blue-300">Image</th>
        <th class="border border-blue-300">Editer</th>
        <th class="border border-blue-300">Supprimer</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($works as $work)
        <tr>
          <td class="px-2 border border-blue-300">{{ $work->id }}</td>
          <td class="tx-9 border border-blue-300">{{ $work->title }}</td>
          <td class="tx-9 border border-blue-300">{{ $work->content }}</td>
          <td class="tx-9 border border-blue-300">
            @include('admin.tags._work_tags', ['tags' => $work->tags])
          </td>
          <td class="tx-9 border border-blue-300"><img src="{{ asset('assets/img/portfolio/'.$work->image) }}" alt="{{ $work->title }}">

          </td>

          <td class="border border-blue-300">
            <a class="edit" href="{{ route('works.edit', $work->id) }}">Edit</a>
          </td>
          <td class="border border-blue-300">
                <form action="{{ route('works.destroy', $work->id)}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button class="" type="submit">Delete</button>
                </form>
          </td>

        </tr>
      @endforeach
    </tbody>
  </table>

@endsection
