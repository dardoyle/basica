{{-----------------------------------------------------------------
 ./ressources/views/admin/works/editForm.blade.php
 affichage du formulaire d'édition d'un work

 Variables disponibles :
 $work (ojet de type Work)
 $client (objet de type Client)
 $tags (objets de type Tag)
---------------------------------------------------------------}}

@extends('admin.dashboard')

@section('title')
  modification d'un work
@endsection

@section('content')

  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('works.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>

  <form method="post" action="{{ route('works.update', $work->id ) }}">
      <div>
          @csrf
          @method('PATCH')
          <label class="mr-11" for="title">Title</label>
          <input class="m-6" type="text" name="title" id="title" value="{{ $work->title }}" placeholder="titre du work"/><br/>
          <label class="mr-11" for="content">Content</label>
          <textarea name ="content" id="content"  rows="3"  cols="30" placeholder="{{ $work->content }}"></textarea><br/>
          <div>
            <label class="mr-14" for="client">Client</label>
            <select class="mt-4" name="client" id="client">
              @foreach ($clients as $client)
                  <option value="{{ $client->id }}" {{ ( $client->id == $work->client_id) ? 'selected' : null }}>{{ $client->name }} </option>
              @endforeach
            </select>
          </div>

          @php
            $checkedTags = [];
            $longueur = count($work->tags);
            for($i = 0; $i < $longueur; $i++){
              $checkedTags[$i] = $work->tags[$i]->id;
            }

          @endphp
          <fieldset class="mt-5">
            <legend>Tags</legend>
            <div class="m-6 mt-0">
              @foreach ($tags as $tag)
                <input class = "ml-20 mr-3"
                       type="checkbox"
                       name="tags[]"
                       value="{{ $tag->id }}" {{ (in_array($tag->id, $checkedTags)) ? 'checked="checked"' : ' ' }}>{{ $tag->name }} <br/>
              @endforeach
            </div>
          </fieldset>

          <label class="mr-14"for="image">Image</label>
          <input class="mt-5" type="text" name="image" id="image" value="{{ $work->image }}" placeholder="lien vers l'image du post">
        <button class="m-2 rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Valider</button>
      </div>
  </form>
@endsection
