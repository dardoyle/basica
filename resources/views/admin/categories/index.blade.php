{{-----------------------------------------------------------------
 ./ressources/views/admin/categories/index.blade.php
 liste des catégories

 Variables disponibles : $categories (tableau d'ojets de type Categorie)
------------------------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  liste des catégories
@endsection

@section('content')

  <table class="border-separate border border-blue-300 table-fixed">
    <thead>
      <tr>
        <th class="px-2 border border-blue-300">#</th>
        <th class="pr-96 border border-blue-300">Nom</th>
        <th class="px-10 border border-blue-300">Editer</th>
        <th class="px-10 border border-blue-300">Supprimer</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($categories as $categorie)
        <tr>

          <td class="px-2 border border-blue-300">{{ $categorie->id }}</td>
          <td class="pr-96 tx-9 border border-blue-300">{{ $categorie->name }}</td>
          <td class="px-10 border border-blue-300">
            <a class="edit" href="{{ route('categories.edit', $categorie->id) }}">Edit</a>
          </td>
          <td>
                <form class="px-10 border border-blue-300" action="{{ route('categories.destroy', $categorie->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="" type="submit">Delete</button>
                </form>
          </td>

        </tr>
      @endforeach
    </tbody>
  </table>

@endsection
