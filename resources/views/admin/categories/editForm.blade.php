{{-----------------------------------------------------------------
 ./ressources/views/admin/categories/editForm.blade.php
 affichage du formulaire d'édition d'une catégorie

 Variable disponible : $categorie (ojet de type Categorie)
---------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  modification d'une catégorie
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('categories.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>

  <form method="post" action="{{ route('categories.update', $categorie->id ) }}">
      <div>
          @csrf
          @method('PATCH')
          <label for="name">Name</label>
          <input type="text" name="name" id="name" value="{{ $categorie->name }}" placeholder="nom de la catégorie"/>

      <button class="m-2 rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Valider</button>
      </div>
  </form>
@endsection
