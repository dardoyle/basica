{{-----------------------------------------------------------------
 ./ressources/views/admin/posts/addForm.blade.php
 affichage du formulaire d'ajout d'un post

 Variable disponible : $categories (tableau d'ojets de type Categorie)
-------------------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  ajout d'un post
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('posts.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>
  <form class="" action="{{ route('posts.store') }}" method="post">
    @csrf
    <h5 class="text-2xl pb-8">Ajout d'un post</h5>
    <label class="mr-11" for="title">Title</label>
    <input class="mr-6" type="text" name="title" id="title" value="" placeholder="titre du post"><br/>
    <label class="mr-11" for="content">Content</label>
    <textarea name ="content" id="content"  rows="3" cols="30" placeholder="contenu du post"></textarea>
    <div>
      <label class="mr-8" for="categorie">Catégorie</label>
      <select class="mt-4" name="categorie" id="categorie">
        <option value="">Veuillez choisir une catégorie</option>
        @foreach ($categories as $categorie)
            <option value="{{ $categorie->id }}" {{ 'selected'}}>{{ $categorie->name }} </option>
        @endforeach
      </select>
    </div>
    <button class="rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Ajouter</button>
  </form>
 </div>
@endsection
