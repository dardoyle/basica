{{-----------------------------------------------------------------
 ./ressources/views/admin/posts/index.blade.php
 liste des posts

 Variable disponible : $posts (tableau d'ojet de type Post)
---------------------------------------------------------------}}

@extends('admin.dashboard')

@section('title')
  liste des posts
@endsection

@section('content')

  <table class="border-separate border border-blue-300 table-fixed">
    <thead>
      <tr>
        <th class="px-2 border border-blue-300">#</th>
        <th class="border border-blue-300">Titre</th>
        <th class="border border-blue-300">Content</th>
        <th class="border border-blue-300">Categorie</th>
        <th class="border border-blue-300">Image</th>
        <th class="border border-blue-300">Editer</th>
        <th class="border border-blue-300">Supprimer</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($posts as $post)
        <tr>
          <td class="px-2 border border-blue-300">{{ $post->id }}</td>
          <td class="tx-9 border border-blue-300">{{ $post->title }}</td>
          <td class="tx-9 border border-blue-300">{{ $post->content }}</td>
          {{-- CATEGORIES : MENU DEROULANT DYNAMIQUE --}}
          <td class="tx-9 border border-blue-300">
            {{ $post->categorie->name }}</td>
          <td class="tx-9 border border-blue-300"><img src="{{ asset('assets/img/blog/'.$post->image) }}" alt="{{ $post->title }}">

          </td>

          <td class="border border-blue-300">
            <a class="edit" href="{{ route('posts.edit', $post->id) }}">Edit</a>
          </td>
          <td class="border border-blue-300">
                <form action="{{ route('posts.destroy', $post->id)}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <button class="" type="submit">Delete</button>
                </form>
          </td>

        </tr>
      @endforeach
    </tbody>
  </table>
  <div class="pagination-wrapper ">
    {{ $posts->links() }}
  </div>

@endsection
