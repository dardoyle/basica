{{-----------------------------------------------------------------
 ./ressources/views/admin/posts/editForm.blade.php
 affichage du formulaire d'édition d'un post

 Variable disponible :
 $post (objet de type Post)
 $categories (tableau d'ojet de type Categorie)
---------------------------------------------------------------}}

@extends('admin.dashboard')

@section('title')
  modification d'un post
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('posts.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>


  <form method="post" action="{{ route('posts.update', $post->id ) }}">
      <div>
          @csrf
          @method('PATCH')
          <label class="mr-11" for="title">Title</label>
          <input class="m-6" type="text" name="title" id="title" value="{{ $post->title }}" placeholder="titre du post"/><br/>
          <label class="mr-11" for="content">Content</label>
          <textarea name ="content" id="content"  rows="3"  cols="30" placeholder="{{ $post->content }}"></textarea>
          <!-- CATEGORIES : MENU DEROULANT DYNAMIQUE -->
          <div>
            <label class="mr-8" for="categorie">Catégorie</label>
            <select class="mt-4" name="categorie" id="categorie">
              @foreach ($categories as $categorie)
                  <option value="{{ $categorie->id }}" {{ ( $categorie->id == $post->categorie_id) ? 'selected' : null }}>{{ $categorie->name }} </option>
              @endforeach
            </select>
          </div>
          <label class="mr-14"for="image">Image</label>
          <input class="mt-5" type="file" name="image" id="image" value="{{ $post->image }}" placeholder="lien vers l'image du post">
        <button class="m-2 rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Valider</button>
      </div>
  </form>
@endsection
