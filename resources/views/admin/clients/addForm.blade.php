{{-----------------------------------------------------------------
 ./ressources/views/admin/clients/addForm.blade.php
 affichage du formulaire d'édition d'un client
---------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  ajout d'un client
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('clients.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>
  <form class="" action="{{ route('clients.store') }}" method="post">
    @csrf
    <h5 class="text-2xl pb-8">Ajout d'un client</h5>
    <label for="name">Name</label>
    <input type="text" name="name" id="name" value="" placeholder="nom du client">
    <button class="rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Ajouter</button>
  </form>
 </div>
@endsection
