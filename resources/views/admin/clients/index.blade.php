{{-----------------------------------------------------------------
 ./ressources/views/admin/cclients/editForm.blade.php
 affichage du formulaire d'édition d'un client

 Variable disponible : $client (ojet de type Client)
---------------------------------------------------------------}}

@extends('admin.dashboard')

@section('title')
  liste des clients
@endsection

@section('content')

  <table class="border-separate border border-blue-300 table-fixed">
    <thead>
      <tr>
        <th class="px-2 border border-blue-300">#</th>
        <th class="pr-96 border border-blue-300">Nom</th>
        <th class="px-10 border border-blue-300">Editer</th>
        <th class="px-10 border border-blue-300">Supprimer</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($clients as $client)
        <tr>

          <td class="px-2 border border-blue-300">{{ $client->id }}</td>
          <td class="pr-96 tx-9 border border-blue-300">{{ $client->name }}</td>
          <td class="px-10 border border-blue-300">
            <a class="edit" href="{{ route('clients.edit', $client->id) }}">Edit</a>
          </td>
          <td>
                <form class="px-10 border border-blue-300" action="{{ route('clients.destroy', $client->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="" type="submit">Delete</button>
                </form>
          </td>

        </tr>
      @endforeach
    </tbody>
  </table>

@endsection
