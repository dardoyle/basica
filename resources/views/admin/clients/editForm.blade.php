{{-----------------------------------------------------------------
 ./ressources/views/admin/clients/editForm.blade.php
 affichage du formulaire d'édition d'un client

 Variable disponible : $client (ojet de type Client)
---------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  modification d'un client
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('clients.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>

  <form method="post" action="{{ route('clients.update', $client->id ) }}">
      <div>
          @csrf
          @method('PATCH')
          <label for="name">Name</label>
          <input type="text" name="name" id="name" value="{{ $client->name }}" placeholder="nom du client"/>

      <button class="m-2 rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Valider</button>
      </div>
  </form>
@endsection
