{{-----------------------------------------------------------------
 ./ressources/views/admin/tags/addForm.blade.php
 affichage du formulaire d'ajout d'un tag
---------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  ajout d'un tag
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('tags.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>
  <form class="" action="{{ route('tags.store') }}" method="post">
    @csrf
    <h5 class="text-2xl pb-8">Ajout d'un tag</h5>
    <label for="name">Name</label>
    <input type="text" name="name" id="name" value="" placeholder="nom du client">
    <button class="rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Ajouter</button>
  </form>
 </div>
@endsection
