{{-----------------------------------------------------------------
 ./ressources/views/admin/tags/index.blade.php
 liste des tags

 Variable disponible : $tags (ojet de type Tag)
---------------------------------------------------------------}}

@extends('admin.dashboard')

@section('title')
  liste des tags
@endsection

@section('content')

  <table class="border-separate border border-blue-300 table-fixed">
    <thead>
      <tr>
        <th class="px-2 border border-blue-300">#</th>
        <th class="pr-96 border border-blue-300">Nom</th>
        <th class="px-10 border border-blue-300">Editer</th>
        <th class="px-10 border border-blue-300">Supprimer</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tags as $tag)
        <tr>

          <td class="px-2 border border-blue-300">{{ $tag->id }}</td>
          <td class="pr-96 tx-9 border border-blue-300">{{ $tag->name }}</td>
          <td class="px-10 border border-blue-300">
            <a class="edit" href="{{ route('clients.edit', $tag->id) }}">Edit</a>
          </td>
          <td>
                <form class="px-10 border border-blue-300" action="{{ route('clients.destroy', $tag->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="" type="submit">Delete</button>
                </form>
          </td>

        </tr>
      @endforeach
    </tbody>
  </table>

@endsection
