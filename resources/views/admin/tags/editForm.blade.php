{{-----------------------------------------------------------------
 ./ressources/views/admin/tags/editForm.blade.php
 affichage du formulaire d'édition d'un tag

 Variable disponible : $tag (ojet de type Tag)
---------------------------------------------------------------}}
@extends('admin.dashboard')

@section('title')
  modification d'un tag
@endsection

@section('content')
  <a class="pb-6 inline-block text-blue-500 hover:text-blue-800" href="{{ route('tags.index') }}">Retour vers la liste des enregistrements</a>
  <hr/>

  <form method="post" action="{{ route('clients.update', $tag->id ) }}">
      <div>
          @csrf
          @method('PATCH')
          <label for="name">Name</label>
          <input type="text" name="name" id="name" value="{{ $tag->name }}" placeholder="nom du tag"/>

      <button class="m-2 rounded-xl border-solid border-2 border-gray-300 p-1 bg-blue-100" type="submit">Valider</button>
      </div>
  </form>
@endsection
