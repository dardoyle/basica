<?php
/*---------------------------------------------------------
  ./routes/admin/clients.php
-------------------------------------------------------------*/
  use App\Http\Controllers\Clients;
  Route::resource('clients', Clients::class);
