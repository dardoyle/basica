<?php
/*---------------------------------------------------------
  ./routes/admin/categories.php
-------------------------------------------------------------*/
 use App\Http\Controllers\Categories;
 Route::resource('categories', Categories::class);
