<?php
/*---------------------------------------------------------
  ./routes/admin/tags.php
-------------------------------------------------------------*/
  use App\Http\Controllers\Tags;
  Route::resource('tags', Tags::class);
