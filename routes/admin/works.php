<?php
/*---------------------------------------------------------
  ./routes/admin/works.php
-------------------------------------------------------------*/
  use App\Http\Controllers\Works;
  Route::resource('works', Works::class);
