<?php
/*---------------------------------------------------------
  ./routes/admin/posts.php
-------------------------------------------------------------*/
  use App\Http\Controllers\Posts;
  Route::resource('posts', Posts::class);
