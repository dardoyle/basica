<?php
/*---------------------------------------------------------
  ./routes/public/posts.php
-------------------------------------------------------------*/

use App\Http\Controllers\Posts;


// BLOG
// PATTERN: /blog
// CTRL : Posts
// ACTION : index
Route::get('/blog', [Posts::class, 'index'])->name('blog');

// DETAILS D'UN POST
// PATTERN: /posts/post/slug
// CTRL: Posts
// ACTION: show
Route::get('/posts/{post}/{slug}', [Posts::class, 'show'])
    ->where('post', '[1-9][0-9]*')
    ->where('slug', '[a-z0-9][a-z0-9\-]*')
    ->name('posts.show');
