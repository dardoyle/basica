<?php
use App\Http\Controllers\Works;
/*---------------------------------------------------------
  ./routes/public/works.php
-------------------------------------------------------------*/


// DETAILS D'UN TRAVAIL
// PATTERN: /works/work/slug
// CTRL: Posts
// ACTION: show
Route::get('/works/{work}/{slug}', [Works::class, 'show'])
     ->where('work', '[1-9][0-9]*')
     ->where('slug', '[a-z0-9][a-z0-9\-]*')
     ->name('works.show');


// TRAVAUX DU PORTFOLIO
// PATTERN: /portfolio
// CTRL: works
// ACTION: index
Route::get('/portfolio', [Works::class, 'index'])->name('portfolio');


// AJAX MORE WORKS
// PATTERN: /works/ajax/more
// CTRL: Works
// ACTION: more
Route::get('/works/ajax/more/', [Works::class, 'more'])->name('works.ajax.more');
