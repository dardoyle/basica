<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Contacts;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROUTES POUR L'INTERFACE D'ADMINISTRATION
require __DIR__.'/admin/categories.php';
require __DIR__.'/admin/clients.php';
require __DIR__.'/admin/posts.php';
require __DIR__.'/admin/tags.php';
require __DIR__.'/admin/works.php';
// authentification
require __DIR__.'/auth.php';

// ROUTES POUR L'INTERFACE PUBLIQUE
require __DIR__.'/public/posts.php';
require __DIR__.'/public/works.php';

// ROUTE PAR DEFAUT
Route::get('/', function () {
    return view('public.templates.index');
})->name('homepage');

// CONTACT PAGE
// PATTERN: /contact
// CTRL: Contacts
// ACTION: form
Route::get('/contact', [Contacts::class, 'showInfos'])->name('contact');


Route::get('/dashboard', function () {
    return view('admin.home');
})->middleware(['auth'])->name('dashboard');


// Route::post('posts/imageupload', [Posts::class, 'imageUpload'])->name('posts.imageUpload');
//
// Route::post('works/imageupload', [Works::class, 'imageUpload'])->name('works.imageUpload');
