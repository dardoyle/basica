<?php
/*----------------------------------------------------------------------
 ./app/models/Categorie.php
-------------------------------------------------------------*/
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    /**
     * Récupère tous les posts qui ont cette catégorie
     *
     * @return posts la liste des posts
     */
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
}
