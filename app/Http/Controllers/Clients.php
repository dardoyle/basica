<?php
/*--------------------------------------------------
./app/http/controllers/Clients.php
---------------------------------------------------*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client;

class Clients extends Controller
{
  /**
   *  Affichage de la Liste des clients
  */
  public function index(){
    $clients = Client::orderBy('name', 'asc')->get();
    return view('admin.clients.index', compact('clients'));
  }

  /**
   * Affiche le formulaire d'ajout d'un nouveau client
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
     return view('admin.clients.addForm');
  }

  /**
   * Ajout d'un client dans la db.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
       $request->validate([
          'name' => 'required',
           ]);

      Client::create($request->all());

      return redirect()->route('clients.index')
                       ->with('success','Le client a bien été créé.');
  }

  /**
   * Affiche le formulaire d'édition d'un client
   *
   * @param  Client  $client
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
     {
         $client = Client::findOrFail($id);
         return view('admin.clients.editForm', compact('client'));
     }

  /**
   * Mise à jour d'un client dans la db
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  Client  $client
   * @return \Illuminate\Http\Response
   */

   public function update(Request $request, $id)
   {
       $validatedData = $request->validate([
           'name' => 'required',
       ]);
       Client::whereId($id)->update($validatedData);

       return redirect()->route('clients.index')
                        ->with('success','Le client a bien été mise à jour');
   }

  /**
   * Suppression d'un client de la db
   *
   * @param  Client  $client
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
    {
        $client = Client::findOrFail($id);
        $client->delete();

        return redirect()->route('clients.index')
                       ->with('success','Le client a bien été supprimé');
    }

}
