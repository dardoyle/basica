<?php
/*--------------------------------------------------
./app/http/controllers/Works.php
---------------------------------------------------*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Work;
use App\Models\Tag;
use App\Models\Client;

class Works extends Controller
{

  /**
   * Affichage de la liste des works
  */
   public function index(int $limit = 6) {
    $works = Work::orderBy('created_at', 'desc')
                ->take($limit)
                ->get();
    if(request()->routeIs('portfolio')){
      // affichage du portfolio dans l'interface publique
      return view('public.portfolio.index', compact('works'));
    }
    else {
      //$tags = Tag::orderBy('name')->get();
      return view('admin.works.index', compact('works'));
    }
  }

  /**
   * Affichage d'un work
   *
   * @param  Work   $work
   * @return \Illuminate\Http\Response
   */
  public function show (Work $work) {
    return view('public.works.show', compact('work'));
  }

  /**
  * Affichage des works supplémentaires sur click ou sur scroll
  * @param  Request $request
  * @return [tableau d'objets de type Work]
  */
  public function more(Request $request) {

    $limit = (isset($request->limit)) ? $request->limit : 6;

    $works = Work::orderBy('created_at', 'desc')
                 ->take($limit)
                 ->offset($request->offset)
                 ->get();

    return view('public.works._list', compact('works'));

  }

  /**
   * Affiche le formulaire d'ajout d'un nouveau work
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $clients = Client::orderBy('name')->get();
    return view('admin.works.addForm', compact('clients'));
  }

  /**
   * Ajout d'un work dans la db.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $data = $request->all();
      $work = new Work;
      $work->title = $data['title'];
      $work->content = $data['content'];
      $work->client_id = $data['client'];
      $work->save();
      return redirect()->route('works.index')
                       ->with('success','Le work a bien été créé.');
  }

  /**
   * Affiche le formulaire d'édition d'un work
   *
   * @param  Work  $work
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
     {
         $work = Work::findOrFail($id);
         $tags = Tag::orderBy('name')->get();
         $clients = Client::orderBy('name')->get();
         return view('admin.works.editForm', compact('work', 'tags', 'clients'));
     }


   /**
    * Mise à jour d'un work dans la db
    * @param  Request $request [description]
    * @param  Work
    */
   public function update(Request $request, Work $work)
   {
       // suppression des tags (liaisons n-m)

       $work->title = $request->title;
       $work->content = $request->content;
       $work->client_id = $request->client;
       $work->save();

       return redirect()->route('works.index')
                        ->with('success','Le travail a bien été mise à jour');
   }

  /**
   * Suppression d'un work de la db
   *
   * @param  Work  $work
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
    {
        $post = Work::findOrFail($id);
        $post->delete();

        return redirect()->route('works.index')
                       ->with('success','Le travail a bien été supprimé');
    }


  /**
   * Upload d'une image - insertion de l'image
   */
  public function imageUpload(Request $request){
    $request->validate([
      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $imageName = time().'.'.$request->image->extension();
    $request->image->move(public_path('/assets/img/blog/'), $imageName);

    return back()
      ->with('success', 'Votre image a bien été uploadée')
      ->with('image', $imageName);
  }


}
