<?php
/*--------------------------------------------------
./app/http/controllers/Posts.php
---------------------------------------------------*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Categorie;

class Posts extends Controller
{
  /**
   *  Affichage de la Liste des posts
  */

  public function index(int $limit = 4){
      $posts = Post::orderBy('created_at', 'desc')->paginate($limit);
      if(request()->routeIs('blog')){
        // posts affichés dans le blog de l'interface publique
        return view('public.posts.index', compact('posts'));
      }
      else{
        // posts affichés dans l'interface d'administration
        return view('admin.posts.index', compact('posts'));
      }
  }


  /**
   * Affichage d'un post
   *
   * @param  Post   $post
   * @return \Illuminate\Http\Response
   */
  public function show (Post $post) {
    return view('public.posts.show', compact('post'));
  }


  /**
   * Affiche le formulaire d'ajout d'un nouveau post
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
     $categories = Categorie::orderBy('name')->get();
     return view('admin.posts.addForm', compact('categories'));
  }

  /**
   * Ajout d'un post dans la db.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */

  public function store(Request $request){
      $data = $request->all();
      $post = new Post;
      $post->title = $data['title'];
      $post->content = $data['content'];
      $post->categorie_id = $data['categorie'];
      $post->save();
      return redirect()->route('posts.index')
                       ->with('success','Le post a bien été créé.');
      }

  /**
   * Affiche le formulaire d'édition d'un post
   *
   * @param  Post  $post
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
     {
         $post = Post::findOrFail($id);
         $categories = Categorie::orderBy('name')->get();
         return view('admin.posts.editForm', compact('post', 'categories'));
     }


  /**
   * Mise à jour d'un post dans la db
   * @param  Request $request [description]
   * @param  Post
   */
   public function update(Request $request, Post $post)
   {
       $post->title = $request->title;
       $post->content = $request->content;
       $post->categorie_id = $request->categorie;
       $post->save();

       return redirect()->route('posts.index')
                        ->with('success','Le post a bien été mise à jour');
   }

  /**
   * Suppression d'un post de la db
   *
   * @param  Post  $post
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect()->route('posts.index')
                       ->with('success','Le post a bien été supprimé');
    }


  /**
   * Upload d'une image - insertion de l'image
   */
  public function imageUpload(Request $request){
    $request->validate([
      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ]);

    $imageName = time().'.'.$request->image->extension();
    $request->image->move(public_path('/assets/img/blog/'), $imageName);

    return back()
      ->with('success', 'Votre image a bien été uploadée')
      ->with('image', $imageName);
  }

}
