<?php
/*--------------------------------------------------
./app/http/controllers/Tags.php
---------------------------------------------------*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;

class Tags extends Controller
{
  /**
   *  Affichage de la Liste des tags
  */
  public function index(){
    $tags = Tag::orderBy('name', 'asc')->get();
    return view('admin.tags.index', compact('tags'));
  }

  /**
   * Affiche le formulaire d'ajout d'un nouveau tag
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
     return view('admin.tags.addForm');
  }

  /**
   * Ajout d'un tag dans la db.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
       $request->validate([
          'name' => 'required',
           ]);

      Tag::create($request->all());

      return redirect()->route('tags.index')
                       ->with('success','Le tag a bien été créé.');
  }

  /**
   * Affiche le formulaire d'édition d'un tag
   *
   * @param  Tag  $tag
   * @return \Illuminate\Http\Response
   */
   public function edit($id)
     {
         $tag = Tag::findOrFail($id);
         return view('admin.tags.editForm', compact('tag'));
     }

  /**
   * Mise à jour d'un tag dans la db
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  Tag  $tag
   * @return \Illuminate\Http\Response
   */

   public function update(Request $request, $id)
   {
       $validatedData = $request->validate([
           'name' => 'required',
       ]);
       Tag::whereId($id)->update($validatedData);

       return redirect()->route('tags.index')
                        ->with('success','Le tag a bien été mise à jour');
   }

  /**
   * Suppression d'un tag de la db
   *
   * @param  Tag  $tag
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        return redirect()->route('tags.index')
                       ->with('success','Le tag a bien été supprimé');
    }

}
