<?php
/*--------------------------------------------------
./app/http/controllers/Contacts.php
---------------------------------------------------*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Contacts extends Controller
{
  public function showInfos () {
    return view('public.contacts.infos');
  }
}
